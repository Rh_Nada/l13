require('dotenv').config();
const mariadb = require('mariadb');
/** @type {mariadb.PoolConfig} */
const DB_CONF = {
    host: process.env.DB_HOST || "localhost",
    port: parseInt(process.env.DB_PORT, 10) || 3306,
    database: process.env.DB_NAME || "message_app",
    user: process.env.DB_USER || "root",
    password: process.env.DB_PASS || undefined,
    connectionLimit: 5
}

class DB_MSG {
    /**
     * @type {mariadb.Pool}
     * @private
     */
    __pool
    /**
     * @type {mariadb.PoolConnection}
     * @private
     */
    __conn
    constructor() {
        this.__pool = mariadb.createPool(DB_CONF);
    }
    async getConnection() {
        try {
            this.__conn = await this.__pool.getConnection();
        } catch (e) {
            console.error({e});
        }
    }
    /**
     * Store one message into the database.
     * @param {string} content
     * @returns {Promise<any>}
     */
    async storeMessage(content) {
        await this.getConnection();
        const res = await this.__conn.query(
            "INSERT INTO messages(content) VALUES(?)", [content]
        );
        console.log({res});
        await this.release();
    }
    async release() {
        if (this.__conn) await this.__conn.release();
    }
}

const main = () => {
    const db_msg = new DB_MSG();
    db_msg.storeMessage("Hello1")
        .then(() => {
            console.log("Message was inserted.");
            // implement SELECT * FROM messages;
        })
        .catch(() => console.error("Couldn't insert message."));
}
main();

module.exports = { DB_MSG };