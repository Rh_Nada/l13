const server_alert = () => {
	alert('server got message');
};

const send_button = () => {
	const message = document.getElementById('msg').value;
	fetch(`http://localhost:4501/message?msg=${message}`, {
		method: 'GET',
		headers: {
			'Access-Control-Allow-Origin': '*',
			'user-agent': 'vscode-restclient',
			'Content-Type': 'application/json',
		},
	})
		.then((response) => {
			console.log(response);
		})
		.catch((err) => {
			console.error(err);
		});
	console.log('Send message to backend');
};

const refresh_button = () => {
	fetch('http://localhost:4501/all-messages', {
		method: 'GET',
		headers: {
			'Access-Control-Allow-Origin': '*',
			'user-agent': 'vscode-restclient',
			'Content-Type': 'application/json',
		},
	})
		.then((response) => response.json())
		.then((data) => (document.getElementById('demo').innerHTML = data));
};
