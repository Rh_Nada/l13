const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors());

var message = [];
app.get('/', (req, res) => {
	res.send('api working');
});

app.get('/all-messages', (req, res) => {
	if (message.length === 0) {
		res.send(JSON.stringify('No messages'));
	} else {
		res.send(JSON.stringify(message));
	}
});

app.get('/message', async (req, res, next) => {
	console.log(req.params.id);
	message.push(req.query.msg);
	try {
		res.send('msg: ' + req.query.msg);
	} catch (e) {
		console.error('get error', e);
		next;
	}
});

app.get('/new-message', async (req, res, next) => {
	let lastElement = message[message.length - 1];
	res.send('New message:' + lastElement);
});

const PORT = 4501;
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`);
});
